package com.tmall.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author yongxing912
 * @description:
 * @create 2020-03-11 23:47
 */
public class TestTmall {

    public static void main(String[] args) {
        //准备分类测试数据：

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            Connection c= DriverManager.getConnection("jdbc:mysql://localhost:3306/tmall_springboot?useSSL=false" +
                    "&useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai #localhost时启用","root","root456");

            Statement s=c.createStatement();

            String sqlFormat="insert into category values (null,'测试分类%d')";
            for(int i=0;i<=10;i++){
                String sql=String.format(sqlFormat,i);
                s.execute(sql);
            }
            System.out.println("已经成功创建10条分类测试数据");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
