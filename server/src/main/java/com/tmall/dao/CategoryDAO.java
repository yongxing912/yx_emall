package com.tmall.dao;

import com.tmall.pojo.Category;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yongxing912
 * @description:
 * @create 2020-03-11 23:30
 */
public interface CategoryDAO extends JpaRepository<Category,Integer> {
}
