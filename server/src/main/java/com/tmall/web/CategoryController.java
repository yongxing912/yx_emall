package com.tmall.web;

import com.tmall.pojo.Category;
import com.tmall.service.CategoryService;
import com.tmall.util.Page4Navigator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yongxing912
 * @description:
 * @create 2020-03-11 23:38
 */
@RestController
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @GetMapping("/categories")
    public Page4Navigator<Category> list(@RequestParam(value="start",defaultValue = "0") int start,
                               @RequestParam(value="size",defaultValue = "5") int size ){
        start=start<0?0:start;
        Page4Navigator<Category> page=categoryService.list(start,size,5);
        return page;
    }
}
