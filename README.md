how2j网站实战项目

模仿天猫整站SPRINGBOOT版

#### 数据库

tmall_springboot

```mysql
DROP DATABASE IF EXISTS tmall_springboot;
CREATE DATABASE tmall_springboot DEFAULT CHARACTER SET utf8;
USE tmall_springboot;
DROP TABLE IF EXISTS `user`;
CREATE TABLE user(
	id int(11) not null auto_increment,
    name varchar(255) default null,
    password varchar(255) default null,
    salt varchar(255) default null,
    primary key (id)
)ENGINE=innodb DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS category;
CREATE TABLE category(
	id int(11) not null auto_increment,
    name varchar(255) default null,
    primary key (id)
)ENGINE=innodb DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS property;
CREATE TABLE property(
	id int(11) not null auto_increment,
    cid int(11) default null,
    name varchar(255) default null,
    primary key (id),
    CONSTRAINT fk_property_catetory FOREIGN KEY (cid) REFERENCES category (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS product;
CREATE TABLE product(
	id int(11) not null auto_increment,
    name varchar(255) default null,
    subTitle varchar(255) default null,
    originalPrice float default null,
    promotePrice float default null,
    stock int(11) default null,
    cid int(11) DEFAULT NULL,
    createDate datetime default null,
    PRIMARY KEY (id),
    CONSTRAINT fk_product_category FOREIGN KEY (cid) REFERENCES category (id)
)ENGINE=innodb DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS propertyvalue;
CREATE TABLE propertyvalue(
	id int(11) not null auto_increment,
    pid int(11) default null,
    ptid int(11) default null,
    value varchar(255) default null,
    primary key(id),
    CONSTRAINT fk_propertyvalue_property FOREIGN KEY (ptid) REFERENCES property (id),
    CONSTRAINT fk_propertyvalue_product FOREIGN KEY (pid) REFERENCES product (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS productimage;
CREATE TABLE productimage(
	id int(11) NOT NULL AUTO_INCREMENT,
    pid int(11) DEFAULT NULL,
    type varchar(255) DEFAULT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_productimage_product FOREIGN KEY (pid) REFERENCES product (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS review;
CREATE TABLE review(
	id int(11) NOT NULL AUTO_INCREMENT,
    content varchar(4000) DEFAULT NULL,
    uid int(11) DEFAULT NULL,
    pid int(11) DEFAULT NULL,
    createDate datetime DEFAULT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_review_product FOREIGN KEY (pid) REFERENCES product (id),
    CONSTRAINT fk_review_user FOREIGN KEY (uid) REFERENCES user (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS order_;
CREATE TABLE order_(
	id int(11) NOT NULL AUTO_INCREMENT,
    orderCode varchar(255) DEFAULT NULL,
    address varchar(255) DEFAULT NULL,
    post varchar(255) DEFAULT NULL,
    receiver varchar(255) DEFAULT NULL,
    mobile varchar(255) DEFAULT NULL,
    userMessage varchar(255) DEFAULT NULL,
    payDate datetime DEFAULT NULL,
    deliveryDate datetime DEFAULT NULL,
    confirmDate datetime DEFAULT NULL,
    uid int(11) DEFAULT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_order_user FOREIGN KEY (uid) REFERENCES user (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS orderitem;
CREATE TABLE orderitem(
	id int(11) NOT NULL AUTO_INCREMENT,
    pid int(11) DEFAULT NULL,
    oid int(11) DEFAULT NULL,
    uid int(11) DEFAULT NULL,
    number int(11) DEFAULT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_orderitem_user FOREIGN KEY (uid) REFERENCES user (id),
    CONSTRAINT fk_orderitem_product FOREIGN KEY (pid) REFERENCES product (id),
    CONSTRAINT fk_orderitem_order FOREIGN KEY (oid) REFERENCES order_ (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

